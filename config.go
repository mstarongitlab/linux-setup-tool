package main

// The platform to act upon (well, the package manager)
type Platform string

// Flags for the intended uses
type IntendedUse uint

const (
	PLATFORM_FEDORA     = "fedora"     // Package manager dnf
	PLATFORM_SILVERBLUE = "silverblue" // Package manager rpm-ostree & toolbox
	PLATFORM_DEBIAN     = "debian"     // For now just assume debian == ubuntu (all use apt)
	PLATFORM_ARCH       = "arch"       // Package manager pacman (and maybe aur)
)

const (
	USE_GAMING = IntendedUse(1 << iota)
	USE_DEV
)

const (
	DEV_USE_GO = IntendedUse(1 << iota)
	DEV_USE_RUST
	DEV_USE_WEB
)

// Don't include platform in the config since the config is being passed to the relevant function
type Config struct {
	TargetUses IntendedUse
	DevTargets IntendedUse
}
